<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title></title>
         <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

</head>
	<body>

         <header>
             <div  style="text-align: right">
                 <div class="alert alert-dark" role="alert">
                       <li class="navbar-text">${userInfo.name} さん </li>
                      <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
                 </div>
            </div>

        </header>


		<h1><center>ユーザ情報詳細参照<center></h1>

        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    ログインID
                </div>
            <div class="col-sm-6">
               ${user.loginId}
            </div>
        </div></p>


        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    ユーザ名
                </div>
            <div class="col-sm-6">
                ${user.name} 
            </div>
        </div></p>


        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    生年月日
                </div>
            <div class="col-sm-6">
              ${user.birthDate}
            </div>
        </div></p>



        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    登録日時
                </div>
            <div class="col-sm-6">
                ${user.createDate}
            </div>
        </div></p>


        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    更新日時
                </div>
            <div class="col-sm-6">
                  ${user.updateDate}
            </div>
        </div></p>
        <br>
        <br>
        <p><div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-5">
                <a href="UserListServlet">戻る</a>
            <div class="col-sm-6">
        </div></p>


	</body>
</html>