<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title></title>
         <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

</head>
	<body>

         <header>
             <div  style="text-align: right">
                 <div class="alert alert-dark" role="alert">
                    <li class="navbar-text">${userInfo.name} さん </li>
                     <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>

                 </div>
            </div>

        </header>


		<h1><center>ユーザ削除確認<center></h1>

        ログインID: <td> ${delete.loginId} </td><br>
        を本当に削除してよろしいでしょうか。
		

        <center>
       
           	<div> <input type="button" onclick="location.href='UserListServlet'" style="width:100px;" value="キャンセル">
             	<form action="UserDeleteServlet" method="post" style ="display:inline">
             		<input type="hidden" name="id" value="${delete.id}">
             		<input type="submit" style="width:100px;" value="OK">
            	 </form>
          </div>
        </center>




	</body>
</html>