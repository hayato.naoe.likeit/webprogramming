<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title></title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/original/common.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <script src="js/bootstrap.min.js"></script>

         <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>

	<body>
         <header>
             <div  style="text-align: right">
                 <div class="alert alert-dark" role="alert">
                    <li class="navbar-text">${userInfo.name} さん </li>
                      <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
                 </div>
            </div>

        </header>

        <h1><center>ユーザ新規登録</center></h1>
        <c:if test="${errMsg != null}" >
	  		  <div class="alert alert-danger" role="alert">
		 		 ${errMsg}
        		</div>
        		</c:if>
        	<c:if test="${errMsg1 != null}" >
	  		  <div class="alert alert-danger" role="alert">
		 		 ${errMsg1}
        		</div>
        		</c:if>
        		<c:if test="${errMsg2 != null}" >
        		 <div class="alert alert-danger" role="alert">
		 		 ${errMsg2}
        		</div>
        		</c:if>
        		<c:if test="${errMsg3 != null}" >
        		 <div class="alert alert-danger" role="alert">
		 		 ${errMsg3}
        		</div>
        		</c:if>
        		<c:if test="${errMsg4 != null}" >
        		 <div class="alert alert-danger" role="alert">
		 		 ${errMsg4}
        		</div>
        		</c:if>
        		<c:if test="${errMsg5 != null}" >
        		 <div class="alert alert-danger" role="alert">
		 		 ${errMsg5}
        		</div>
        		</c:if>
        		<c:if test="${errMsg6 != null}" >
        		 <div class="alert alert-danger" role="alert">
		 		 ${errMsg6}
        		</div>
        		</c:if>
        		<c:if test="${errMsg7 != null}" >
        		 <div class="alert alert-danger" role="alert">
		 		 ${errMsg7}
        		</div>
			</c:if>
        
        
<form action="UserInsertServlet" method="post">

        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    ログインID
                </div>
            <div class="col-sm-6">
                <input type="text" name="loginId" style="width:200px;">
            </div>
        </div></p>



        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    パスワード
                </div>
            <div class="col-sm-6">
                <input type="password" name="password" style="width:200px;">
            </div>
        </div></p>



        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    パスワード（確認）
                </div>
            <div class="col-sm-6">
                <input type="password" name="password2" style="width:200px;">
            </div>
        </div></p>



        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                     ユーザ名
                </div>
            <div class="col-sm-6">
                <input type="text" name="name" style="width:200px;">
            </div>
        </div></p>



        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    生年月日
                </div>
            <div class="col-sm-6">
                <input type="date" name="birthDate" style="width:200px;">
            </div>
        </div></p>

        <br>
        <br>
        <p><div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-5">
               <a href="UserListServlet" class="navbar-link userall-link">戻る</a>
            </div>
        <div class="col-sm-6">
        </div>
        </div></p>


        <center>
            <input type="submit"  value="登録">
        </center>

	   </form>
	</body>
</html>