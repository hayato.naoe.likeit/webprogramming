<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/original/common.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <script src="js/bootstrap.min.js"></script>
    
         <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

</head>
	<body>
		<h1><center>ログイン画面<center></h1>
           
        <c:if test="${errMsg != null}" >
		 <div class="alert alert-danger" role="alert">
		${errMsg}
		</div>
	</c:if>
    <form  action="LoginServlet" method="post"> 
          <br>
            <br> 
        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    ログインID
                </div>
            <div class="col-sm-6">
                <input type="text" name="loginId" style="width:200px;">
            </div>
        </div></p>



        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    パスワード
                </div>
            <div class="col-sm-6">
                <input type="password" name="password" style="width:200px;">
            </div>
        </div></p>
        <br>
        <center>
            <input type="submit" onclick="UserDao" value="ログイン">
        </center>

</form>
	</body>
</html>
