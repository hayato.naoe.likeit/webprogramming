<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン</title>
         <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

</head>
         <header>
             <div  style="text-align: right">
                <div class="alert alert-dark" role="alert">
                    <li class="navbar-text">${userInfo.name} さん </li>
                    <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
                 </div>
            </div>

        </header>


        <div style="text-align:right">
           <a href="UserInsertServlet">新規登録</a>
        </div>


        <h1><center>ユーザ一覧</center></h1>
         <form action="UserListServlet" method="post">
        <center>
        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-3">
                    ログインID
                </div>
            <div class="col-sm-8">
                <input type="text" name="loginId" style="width:300px;" class="form-control">
            </div>
        </div></p>


        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-3">
                    ユーザ名
                </div>
            <div class="col-sm-8">
                <input type="text" name="name" style="width:300px;">
            </div>
        </div></p>


        <p><div class="row">
            <div class="col-sm-1"></div>
                <div class="col-sm-3">
                    生年月日
                </div>
            <div class="col-sm-8">
                <input type="date" name="birthDate" style="width:125px;">　〜　
                <input type="date" name="birthDate2" style="width:128px;">
            </div>
        </div></p>



        <p><div class="row">
            <div class="col-sm-9"></div>
            <div class="col-sm-3">
                <input type="submit" value="検索">
            </div>
        </div></p>
        	 </form>



        <dr>
        <center>
        <table border="1">



            <tr align="center">
                <th width="90">ログインID</th>
                <th width="90">ユーザ名</th>
                <th width="150">生年月日 </th>
                <th width="200"> </th>
            </tr>
            
            <form action="UseListServlet" method="post">
            <c:forEach var="user" items="${userList}" >
            <tr>
                <td>${user.loginId}</td>
                <td>${user.name}</td>
                <td>${user.birthDate}</td>
                <td><center>
                    <input type="button" value="詳細" onclick="location.href='UserDetailServlet?id=${user.id}'" style=color:white;background-color:blue>
                   <c:if test ="${userInfo.loginId == 'admin' || userInfo.loginId == user.loginId}"> 
                   <input type="button" value="更新" onclick="location.href='UserUpdateServlet?id=${user.id}'" style=color:white;background-color:greenyellow>
                    </c:if>
                    <c:if test ="${userInfo.loginId == 'admin'}"> 
                    <input type="button" value="削除" onclick="location.href='UserDeleteServlet?id=${user.id}'" style=color:white;background-color:red>
                     </c:if>
                </center> </td>
            </tr>
            </c:forEach>
         </table>
        </center>
        </dr>
	 </form>
	</body>
</html>
