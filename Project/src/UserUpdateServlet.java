

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import modle.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		if(session == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/1login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		String id = request.getParameter("id");
		System.out.println(id);

		UserDao dao = new UserDao();
		User userInfo =  dao.info(id);


		System.out.println(userInfo.getName());
		System.out.println(userInfo.getLoginId());
		System.out.println(userInfo.getBirthDate());
		System.out.println(userInfo.getCreateDate());
		System.out.println(userInfo.getUpdateDate());


		request.setAttribute("use", userInfo);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/5infoUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

	String id = request.getParameter("id");
	String loginId = request.getParameter("loginId");
	String password = request.getParameter("password");
	String password2 = request.getParameter("password2");
	String name = request.getParameter("name");
	String birthDate = request.getParameter("birthDate");

	System.out.println(id);
	System.out.println(password);
	System.out.println(name);
	System.out.println(birthDate);

	UserDao dao = new UserDao();


	User userInfo = dao.info(id);



	 if((name.equals("")|| name.length()==0)
			 
			 ||( birthDate.equals("")|| birthDate.length()==0)
			 ||(!password.equals(password2))) {

	    	request.setAttribute("errMsg","入力された内容は正しくありません");
	    	request.setAttribute("use", userInfo);


	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/5infoUpdate.jsp");
			dispatcher.forward(request, response);
			return;
			}

	    dao.update(id, password, name, birthDate);

	    response.sendRedirect("UserListServlet");
	}
}
