

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import modle.User;

/**
 * Servlet implementation class UserInsertServlet
 */
@WebServlet("/UserInsertServlet")
public class UserInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInsertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		

		HttpSession session = request.getSession(false);
		if(session != null && session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/1login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/3userNew.jsp");
		dispatcher.forward(request, response);



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unlikely-arg-type")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");


	String loginId = request.getParameter("loginId");
	String password = request.getParameter("password");
	String password2 = request.getParameter("password2");
	String name = request.getParameter("name");
	String birthDate = request.getParameter("birthDate");


	UserDao dao = new UserDao();
	
	//-------------------------------
	List<User> list = dao.findAll();
	
	if(loginId.equals(list)) {
    	request.setAttribute("errMsg", "すでに登録されたログインIDです");

    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/3userNew.jsp");
		dispatcher.forward(request, response);
		return;

    }
	//----------------------------------

	 if(loginId.equals("")|| loginId.length()==0) {
	    	request.setAttribute("errMsg1", "ログインIDが入力されていません");

	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/3userNew.jsp");
			dispatcher.forward(request, response);
			return;

	    }
	    if(name.equals("")|| name.length()==0) {
	    	request.setAttribute("errMsg2","ユーザー名が入力されていません");

	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/3userNew.jsp");
			dispatcher.forward(request, response);
			return;

	    }
	    if(password.equals("")|| password.length()==0) {
	    	request.setAttribute("errMsg3","パスワードが入力されていません");
	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/3userNew.jsp");
			dispatcher.forward(request, response);
			return;

	    }
	    if(password2.equals("")|| password2.length()==0) {
	    	request.setAttribute("errMsg4","パスワード(確認)が入力されていません");
	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/3userNew.jsp");
			dispatcher.forward(request, response);
			return;

	    }
	    if( birthDate.equals("")|| birthDate.length()==0) {
	    	request.setAttribute("errMsg5","生年月日が選択されていません");
	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/3userNew.jsp");
			dispatcher.forward(request, response);
			return;

	    }
	    if(!password.equals(password2)) {
		    request.setAttribute("errMsg6","入力パスワードが一致しません");
		    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/3userNew.jsp");
			dispatcher.forward(request, response);
			return;

	    }

	    dao.insertUser(loginId, password, name, birthDate);


	    response.sendRedirect("UserListServlet");

	}
}