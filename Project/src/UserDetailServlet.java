

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import modle.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		
		//--暗号化
		HttpSession session = request.getSession(false);
		if(session != null && session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/1login.jsp");
			dispatcher.forward(request, response);
			return;
		}
		//--暗号化end
		
		
		String id = request.getParameter("id");

		System.out.println(id);




		UserDao dao = new UserDao();
		User userInfo =  dao.info(id);


		System.out.println(userInfo.getName());
		System.out.println(userInfo.getLoginId());
		System.out.println(userInfo.getBirthDate());
		System.out.println(userInfo.getCreateDate());
		System.out.println(userInfo.getUpdateDate());
		






		request.setAttribute("user", userInfo);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/4userInfo.jsp");
		dispatcher.forward(request, response);

	}





	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */


}
