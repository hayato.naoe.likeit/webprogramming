package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.xml.bind.DatatypeConverter;

import modle.User;


/**
 * Servlet implementation class UserDao
 * @param <UserInsertServlet>
 */
@WebServlet("/UserDao")
public class UserDao {


public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
   UserDao dao = new UserDao();
	
    
    try {
        conn = DBmanageger.getConnection();

        String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, loginId);
        pStmt.setString(2, dao.lock(password));
        ResultSet rs = pStmt.executeQuery();

        if (!rs.next()) {
            return null;
        }

        String loginIdData = rs.getString("login_id");
        String nameData = rs.getString("name");
        return new User(loginIdData, nameData);

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}



public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
        conn = DBmanageger.getConnection();

        String sql = "SELECT * FROM user";

        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);


        while (rs.next()) {
            int id = rs.getInt("id");
            String loginId = rs.getString("login_id");
            String name = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

            userList.add(user);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {


        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }

    }
    return userList;
}

//-------------------------------------------------------------------------
public void insertUser(String loginId, String password, String name, String birthDate) {
	Connection conn = null;
	PreparedStatement stmt = null;
	UserDao dao = new UserDao();

	String insertSQL = "insert into user (login_id,password,name,birth_date,create_date,update_date) VALUES(?,?,?,?,now(),now())";


	try {

		conn = DBmanageger.getConnection();
		stmt = conn.prepareStatement(insertSQL);



		stmt.setString(1,loginId);
		stmt.setString(2, dao.lock(password));
		stmt.setString(3, name);
		stmt.setString(4, birthDate);

		
		stmt.executeUpdate();


	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {

			if (stmt != null) {
				stmt.close();
			}

			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}


//-----------------------------------------------------------
public User info(String id) {
	Connection conn = null;



	 try {
	        conn = DBmanageger.getConnection();



	        String sql = "SELECT * from user where id = ?";

	        PreparedStatement pStmt = conn.prepareStatement(sql);
	         pStmt.setString(1, id);
	        ResultSet rs = pStmt.executeQuery();

	        if (!rs.next()) {
	            return null;
	        }

	        int id2 = rs.getInt("id");
            String loginId = rs.getString("login_id");
            String name = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            User user = new User(id2, loginId, name, birthDate, password, createDate, updateDate);

            return user;

	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {


	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }

	        }

	    }
	}

//------------------------------------------------------------------
public void update(String id,String password, String name, String birthDate) {
	Connection conn = null;
	PreparedStatement stmt = null;
	UserDao dao = new UserDao();
	

	 

	 try {
			conn = DBmanageger.getConnection();
			
		if(password.equals("")) {
			String sql = "update user set name = ?,birth_date = ?,update_date = now() where id = ?";
			stmt = conn.prepareStatement(sql);

			
			stmt.setString(1, name);
			stmt.setString(2, birthDate);
			stmt.setString(3,id);
			
		}	else {
			
			String sql = "update user set password = ?,name = ?,birth_date = ?,update_date = now() where id = ?";
			stmt = conn.prepareStatement(sql);

			stmt.setString(1, dao.lock(password));
			stmt.setString(2, name);
			stmt.setString(3, birthDate);
			stmt.setString(4,id);
		}

			stmt.executeUpdate();


	 } catch (SQLException e) {
		 e.printStackTrace();

		} finally {
			try {

				if (stmt != null) {
					stmt.close();
				}

				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

//--------------------------------------------------------------
public User delete(String id) {

	Connection conn = null;
	PreparedStatement stmt = null;

	String sql = "delete from user where id = ?";

	 try {
			conn = DBmanageger.getConnection();

			stmt = conn.prepareStatement(sql);
			stmt.setString(1,id);

			stmt.executeUpdate();


	} catch (SQLException e) {
		 e.printStackTrace();

		} finally {
			try {

				if (stmt != null) {
					stmt.close();
				}

				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	return null;
	}



//------------------------------------------------------------------
public List<User> find(String loginId ,String name,String birthDate,String birthDate2) {
	 	Connection conn = null;
	    List<User> userList = new ArrayList<User>();

	    try {
	        conn = DBmanageger.getConnection();


	        String sql= "SELECT * FROM user where login_id != 'admin'";
  

	         if(!loginId.equals("")){
	        	 sql += " and login_id = '" + loginId + "'";  
	         
	         }
	         if(!name.equals("")){
	        	 sql += " and name like '%" +name+ "%'";
	        	
	         }
	         if(!birthDate.equals("")){
	        	 sql += " and birth_date >= '" + birthDate + "'";
	        	 
	         }if(!birthDate2.equals("")){
	        	 sql += " and birth_date <= '" + birthDate2 + "'";
	         }
	        	
	         PreparedStatement pStmt = conn.prepareStatement(sql);

	         System.out.println(sql);

	        ResultSet rs = pStmt.executeQuery();
	        

	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String loginId1 = rs.getString("login_id");
	            String name1 = rs.getString("name");
	            Date birthDate1 = rs.getDate("birth_date");
	            String password = rs.getString("password");
	            String createDate = rs.getString("create_date");
	            String updateDate = rs.getString("update_date");
	            User user = new User(id, loginId1, name1, birthDate1, password, createDate, updateDate);

	            userList.add(user);
	       
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {


	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }

	    }
	    return userList;
	}

public String lock(String password) {	
	//ハッシュを生成したい元の文字列
	String source = password;
	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";

	//ハッシュ生成処理
	byte[] bytes = null;
	try {
		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	} catch (NoSuchAlgorithmException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	}
	String result = DatatypeConverter.printHexBinary(bytes);
	//標準出力
	System.out.println(result);
	return result;
	
	

}

}