package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;

/**
 * Servlet implementation class employee
 */
@WebServlet("/employee")
public class employee {
	 private int id;
	 private String loginId;
	 private String name;
	 private Date birthDate;
	 private String password;
	 private String createDate;
	 private String updateDate;
	
	
	public employee(int id ,String loginId, String name, Date birthDate, String password, String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
		
		
	}
	public  void save() throws SQLException {
		
	Connection conn = null;
	PreparedStatement stmt = null;
	
	
	try {
		
		conn = DBmanageger.getConnection();
		
		String insertSQL = "insert into user (loginId,password,name,birthDate) VALUES(?,?,?,?)";
		stmt = conn.prepareStatement(insertSQL);
		
		
		
		stmt.setString(1,this.loginId);
		stmt.setString(2, this.password);
		stmt.setString(3, this.name);
		stmt.setDate(4, this.birthDate);
		
		stmt.executeUpdate();
		
	
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			
			if (stmt != null) {
				stmt.close();
			}
			
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	}
}



